
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy.linalg import norm


class Regularized_phaseret_object:
    """
    This is the argument of the projection in Step 3 of Algorithm 4.1 of 
    "Proximal Heterogeneous Block Implicit-Explicit Method and 
    Application to Blind Ptychographic Diffraction Imaging", 
    R. Hesse, D. R. Luke, S. Sabach and M. K. Tam, 
    SIAM J. on Imaging Sciences, 8(1):426--457 (2015).
  
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on 1st Sept 2017.
    """

    def __init__(self, experiment):
        # the stepsize parameter gamma is only needed for the analysis.
        # For the implmentation, this can be taken to be machine zero.
        if hasattr(experiment, 'gamma'):
            self.gamma = experiment.gamma
        else:
            self.gamma = 1e-30


    def eval(self, u, prox_idx=None):
        """
        Evaluate this explicit prox operator

        Parameters
        ----------
        u : Cell
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        tmp_u : Cell
            The projection.
        """
        tmp_u = Cell(len(u[1]))
        for j in range(len(tmp_u)):
            tmp_u[j] = 2/(2+self.gamma)*u[0] + self.gamma/(2 + self.gamma)* u[1][j]
        return tmp_u

