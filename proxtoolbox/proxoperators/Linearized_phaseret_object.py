
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy.linalg import norm


class Linearized_phaseret_object:
    """
    Explicit step with respect to the object variable of the 
    objective given in Eq(2.5) of  "Proximal Heterogeneous 
    Block Implicit-Explicit Method and Application to Blind
    Ptychographic Diffraction Imaging", 
    R. Hesse, D. R. Luke, S. Sabach and M. K. Tam, 
    SIAM J. on Imaging Sciences, 8(1):426--457 (2015).
    The last block is the z block in the paper above.
  
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on 1st Sept 2017.
    """

    def __init__(self, experiment):
        self.indicator_ampl = experiment.indicator_ampl
        if hasattr(experiment, 'overrelax'):
            self.overrelax = experiment.overrelax
        else:
            self.overrelax = 1.0
        # Sets the PHeBIE block stepsizes
        if hasattr(experiment, 'beta'):
            self.beta = experiment.beta
        else:
            self.beta = np.ones(experiment.nProx-1) # TODO check if correct


    def eval(self, u, prox_idx=None):
        """
        Evaluate this explicit prox operator

        Parameters
        ----------
        u : Cell
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        tmp_u_s : Cell
            The projection.
        """
        s = prox_idx  # was block_idx in Matlab
        tmp_u_s = u[s].copy()
        z = u[len(u)-1]
        m, n, p, _q = size_matlab(tmp_u_s)  # assumes this is an array
        mz, nz, pz, _qz = size_matlab(z[0])
        xSum = pz * self.indicator_ampl
        # the probe is absorbed in the other block of variables, z, which 
        # are constrained to lie in the phase sets.  So, effectively, 
        # the probe is just the indicator function of the aperature, 
        # which simplifies the representations:
        if isCell(z):
            phiSum = np.zeros((m, n), dtype=z[0].dtype)
            for j in range(len(z)):
                phiSum += z[j]
        else:
            phiSum = np.zeros((m, n, p), dtype=z.dtype)
            if pz > p:
                for j in range(pz):
                    phiSum += z[:,:,j]
            elif pz == 1 and nz > n:
                phiSum = np.sum(z, axis=0) # TODO check if axis=0 is correct
            elif pz == 1 and mz > m:
                phiSum = np.sum(z, axis=1) # TODO check if axis=1 is correct

        xsum_denom = self.overrelax * self.beta[s] * np.maximum(xSum, 1e-30)
        tmp_u_s -= (xSum*tmp_u_s - phiSum) / xsum_denom
        return tmp_u_s
