from numpy import zeros_like, unravel_index, sum, max
import numpy as np
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.orbitaltomog import tile_array, bin_array
from proxtoolbox.utils.size import size_matlab

__all__ = ['P_Sparsity', 'P_Sparsity_real', 'P_Sparsity_Symmetric', 'P_Sparsity_Symmetric_real',
           'P_Sparsity_Superpixel', 'P_Sparsity_Superpixel_real', 'P_nonneg_sparsity']


class P_Sparsity(ProxOperator):
    """
    Projection subroutine for projecting onto a sparsity constraint
    """

    def __init__(self, experiment):
        """
        Initialization

        Parameters
        ----------
        experiment : class with the attribute 'sparsity_parameter', which should be an integer
        indicating the number of pixels allowed by the projection
        """
        super(P_Sparsity, self).__init__(experiment)
        self.sparsity_parameter = experiment.sparsity_parameter

        if hasattr(experiment, 'use_sparsity_with_support') and experiment.use_sparsity_with_support:
            self.support = experiment.sparsity_support.real.astype(np.uint)
            assert (sum(self.support) > 0), 'Invalid empty sparsity_support given'
        else:
            self.support = 1

        if self.sparsity_parameter > 30 or len(experiment.u0.shape) != 2:
            def value_selection(original, indices, sparsity_parameter):
                idx_for_threshold = unravel_index(indices[-sparsity_parameter], original.shape)
                threshold_val = abs(original[idx_for_threshold].get())
                return (abs(original) >= threshold_val) * original
        else:
            def value_selection(original, indices, sparsity_parameter):
                out = zeros_like(original)
                hits = indices[-sparsity_parameter:].get()
                hit_idx = [unravel_index(hit, original.shape) for hit in hits]
                for _idx in hit_idx:
                    out[_idx[0], _idx[1]] = original[_idx[0], _idx[1]]
                return out

        self.value_selection = value_selection

    def eval(self, u, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        u *= self.support  # apply support (simple 1 if no support)
        u_flat = np.ravel(u)  # flatten array before sorting
        sorting = np.argsort(abs(u_flat), axis=None)  # gives indices of sorted array in ascending order
        p_sparse = np.zeros_like(sorting, dtype=u.dtype)  # create array of zeros
        p_sparse[sorting[-1 * int(self.sparsity_parameter):]] = u_flat[sorting[-1 * int(self.sparsity_parameter):]]
        p_sparse = np.reshape(p_sparse, u.shape)  # recreate original shape
        return p_sparse


class P_Sparsity_real(P_Sparsity):
    """
    Projection subroutine for projecting onto a combined sparsity and realness constraint

    order: realness, sparsity
    """

    def eval(self, u, prox_idx=None):
        return super(P_Sparsity_real, self).eval(u.real, prox_idx)


class P_Sparsity_Symmetric(P_Sparsity):
    """
    Projection subroutine for projecting onto a combined sparsity and symmetry constraint

    order: symmetry, sparsity
    """

    def __init__(self, experiment):
        super(P_Sparsity_Symmetric, self).__init__(experiment)
        self.symmetry = experiment.symmetry_type  # antisymmetric = -1, symmetric = 1
        self.symmetry_axis = experiment.symmetry_axis  # -1 for last, 0 for first, 'both', 'all' or None for full flip

    def eval(self, u, prox_idx=None):
        if self.symmetry_axis in ['both', 'all']:
            mirror = np.flip(u, axis=None)
        else:
            mirror = np.flip(u, axis=self.symmetry_axis)
        inp = (u + self.symmetry * mirror) / 2
        out = super(P_Sparsity_Symmetric, self).eval(inp, prox_idx)
        return out


class P_Sparsity_Symmetric_real(P_Sparsity_Symmetric):
    """
    Projection subroutine for projecting onto a combined symmetry, sparsity and realness constraint

    order: realness, symmetry, sparsity
    """

    def eval(self, u, prox_idx=None):
        out = super(P_Sparsity_Symmetric_real, self).eval(u.real, prox_idx)
        return out


class P_Sparsity_Superpixel(P_Sparsity):
    """
    Apply sparsity on superpixels, i.e. on the binned array
    """

    def __init__(self, experiment):
        super(P_Sparsity_Superpixel, self).__init__(experiment)
        # Set the superpixel size:
        self.superpixel_size = experiment.superpixel_size
        # Bin the support:
        if self.support != 1:
            self.support = bin_array(self.support, self.superpixel_size)
            self.support = self.support / max(self.support)
        # Assert that the binning+upsampling conserves the array size
        test_shape = tile_array(bin_array(experiment.u0, self.superpixel_size, pad_zeros=False),
                                self.superpixel_size).shape
        assert test_shape == experiment.u0.shape, 'Given array size does not allow for binning'
        # TODO: allow for padding, then cut of the remainder after tile_array

    def eval(self, u, prox_idx=None):
        binned = bin_array(abs(u), self.superpixel_size)
        sparse_array = super(P_Sparsity_Superpixel, self).eval(binned, prox_idx)
        mask = tile_array(sparse_array, self.superpixel_size, normalize=True) > 0
        return np.where(mask, u, 0)


class P_Sparsity_Superpixel_real(P_Sparsity_Superpixel):
    """
    Apply real-valued sparsity on superpixels, i.e. on the binned array
    """

    def eval(self, u, prox_idx=None):
        return super(P_Sparsity_Superpixel_real, self).eval(u.real, prox_idx)


class P_nonneg_sparsity(ProxOperator):
    """
    Projection subroutine for projecting onto support constraints
    """

    def __init__(self, experiment):
        super().__init__(experiment)
        self.sparsity_parameter = experiment.sparsity_parameter

    def eval(self, u, prox_idx=None):
        """
        Parameters
        ----------
        u : array_like - Input, array to be projected

        Returns
        -------
        p_Sparsity : array_like, the projection
        """
        m, n, p, q = size_matlab(u)
        null = np.zeros(u.shape)
        tmp = np.maximum(null, np.real(u))
        tmp = np.reshape(tmp, (m * n * p * q), order='F')
        indices = np.argsort(tmp)[::-1]  # descend sort
        p_Sparsity = np.zeros(m * n * p * q)
        p_Sparsity[indices[0:int(self.sparsity_parameter)]] = tmp[indices[0:int(self.sparsity_parameter)]]
        p_Sparsity = np.reshape(p_Sparsity, u.shape, order='F')
        return p_Sparsity
