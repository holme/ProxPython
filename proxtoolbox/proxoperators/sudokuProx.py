from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox import proxoperators
import numpy as np


# Prox operators used by Sudoku experiment

class SudokuProx(ProxOperator):
    """
    Projection onto the given entries in a Sudoku problem
    """

    def __init__(self, experiment):
        self.given = experiment.sudoku.copy()
        self.Nx = experiment.Nx
        self.Ny = experiment.Ny
        self.Nz = experiment.Nz


class ProjGiven(SudokuProx):
    """
    Projection onto the given entries in a Sudoku problem
    """

    def __init__(self, experiment):
        super(ProjGiven, self).__init__(experiment)
        self.given = experiment.sudoku.copy()

    def eval(self, u, prox_idx=None):
        """
        Applies the prox operator to the input data

        Parameters
        ----------
        u : array-like
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        A : array-like
            The projection.
        """
        A = np.zeros((self.Nx, self.Ny, self.Nz), dtype=u.dtype)

        for x in range(self.Nx):
            for y in range(self.Ny):
                z = int(self.given[x, y])
                if z > 0:
                    A[x, y, z - 1] = 1
                else:
                    A[x, y, np.argmax(u[x, y, :])] = 1

        return A


class ProjSquare(SudokuProx):
    """
    Projection onto the given entries in a Sudoku problem
    """

    def __init__(self, experiment):
        super(ProjSquare, self).__init__(experiment)

    def eval(self, u, prox_idx=None):
        """
        Applies the prox operator to the input data

        Parameters
        ----------
        u : array-like
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        Q : array-like
            The projection.
        """
        Q = np.zeros((self.Nx, self.Ny, self.Nz), dtype=u.dtype)
        for z in range(self.Nz):
            for x in range(0, 9, 3):
                for y in range(0, 9, 3):
                    v = np.argmax(u[x:(x + 3), y:(y + 3), z], axis=0)
                    w = np.argmax(np.amax(u[x:(x + 3), y:(y + 3), z], axis=0))

                    Q[x + v[w], y + w, z] = 1
        return Q


class ProjColumn(SudokuProx):
    """
    Projection onto the given entries in a Sudoku problem
    """

    def __init__(self, experiment):
        super(ProjColumn, self).__init__(experiment)

    def eval(self, u, prox_idx=None):
        """
        Applies the prox operator to the input data

        Parameters
        ----------
        u : array-like
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        C : array-like
            The projection.
        """
        C = np.zeros((self.Nx, self.Ny, self.Nz), dtype=u.dtype)
        for x in range(self.Nx):
            for z in range(self.Nz):
                y = np.argmax(u[x, :, z])
                C[x, y, z] = 1
        return C


class ProjRow(SudokuProx):
    """
    Projection onto the given entries in a Sudoku problem
    """

    def __init__(self, experiment):
        super(ProjRow, self).__init__(experiment)

    def eval(self, u, prox_idx=None):
        """
        Applies the prox operator to the input data

        Parameters
        ----------
        u : array-like
            Iterate. 
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        C : array-like
            The projection.
        """
        R = np.zeros((self.Nx, self.Ny, self.Nz), dtype=u.dtype)
        for y in range(self.Ny):
            for z in range(self.Nz):
                x = np.argmax(u[:, y, z])
                R[x, y, z] = 1
        return R
