
from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell

import numpy as np
from numpy.random import rand
from numpy.linalg import norm

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class SourceLocExperiment(Experiment):
    '''
    Source localization experiment class
    '''
 
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'source location',
            'instance': 'simple random',
            'object': 'real',
            'constraint': 'diagonal_and_balls',
            'formulation': 'product space', 
            'product_space_dimension': 1,
            'sensors': 3,
            'phys_dimension': 3,
            'phys_boundary': 100,
            'radius_bound': 100,
            'noise': False,
            'MAXIT': 10000,
            'TOL': 1e-11,
            'lambda_0': 0.33,
            'lambda_max': 0.33,
            'lambda_switch': 1,
            'diagnostic': True,
            'verbose': 0,
            'graphics': 1,
            'anim': False
        }
        return defaultParams


    def __init__(self, 
                 instance='simple random',
                 sensors=3,
                 phys_dimension=3,
                 phys_boundary=100,
                 radius_bound=100,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(SourceLocExperiment, self).__init__(**kwargs)
        # do here any data member initialization
        self.instance = instance
        self.sensors = sensors
        self.phys_dimension = phys_dimension
        self.phys_boundary = phys_boundary
        self.radius_bound = radius_bound
        # internal data members
        self.shift_data = None # default receiver location
 
    def loadData(self):
        """
        Load source localization dataset. Create the initial iterate.
        """

        self.Nx = 1
        self.Ny = self.phys_dimension
        # old code
        #self.Nx = self.phys_dimension
        #self.Ny = self.sensors
        self.Nz = 1

        # we load data depending on the instance parameter

        if self.instance == 'simple random':
            dimension = self.phys_dimension
            sensors = self.sensors
            # generate the true position of the source
            self.truth = 2*self.phys_boundary*(rand(1, dimension) - 0.5)
            self.truth_dim = self.truth.shape
            self.norm_truth = norm(self.truth)
            # generate the sensors
            # each row corresponds to a sensor
            # (this reshaping procedure is done so that we get the same 
            # random numbers as Matlab)
            sensorMatrix = 2*self.phys_boundary*(rand(sensors*dimension) - 0.5)
            sensorMatrix = np.reshape(sensorMatrix, (sensors, dimension), order='F')
            # convert to cells
            self.shift_data = Cell(sensors)
            for i in range(sensors):
                self.shift_data[i] = sensorMatrix[i]
            self.receiver_data_scale = self.phys_boundary*sensors*dimension
            # calculate the distances from the source to the sensors
            self.data = np.zeros((sensors))
            for i in range(sensors):
                self.data[i] = norm(self.shift_data[i] - self.truth)
                if self.noise:
                    self.data[i] += 20*(rand(1)-0.5) 
            self.norm_data = norm(self.data)
            # initial guess
            self.u0 = self.truth.flatten() + 0.1*self.phys_boundary*(rand(dimension)-0.5)
        else:
            errMsg = "Loading data: instance " + self.instance \
                      + " is not yet implemented"
            raise NotImplementedError(errMsg)

        if self.formulation == "product space":
            u0 = self.u0
            self.u0 = Cell(self.sensors)
            for j in range(self.sensors):
                self.u0[j] = u0.copy()



    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(SourceLocExperiment, self).setupProxOperators() # call parent's method
        
        # new code:
        # 
        if self.formulation == 'cyclic':
            self.iterate_monitor_name = 'FeasibilityIterateMonitor'
            self.product_space_dimension = 1
            self.proxOperators = []
            self.productProxOperators = []
            self.sets = self.sensors 
            self.nProx = self.sensors
            for _j in range(self.nProx):
                self.proxOperators.append('Project_on_sphere')
        elif self.formulation == 'product space':
            self.iterate_monitor_name = 'FeasibilityIterateMonitor'
            self.product_space_dimension = self.sensors
            self.sets = 2
            self.nProx = 2
            self.proxOperators.append('P_diag')
            self.proxOperators.append('Prox_product_space')
            # add product prox operators
            self.n_product_Prox = self.product_space_dimension
            for _j in range(self.n_product_Prox):
                self.productProxOperators.append('Project_on_sphere')
        elif self.formulation == 'NSLS':
            self.optimality_monitor='NSLS_sourceloc_objective'
            if self.algorithm_name == 'AvP2':
                self.iterate_monitor_name = 'AvP2_IterateMonitor'
                self.Prox = 2
                self.product_space_dimension = self.sensors
                self.n_product_Prox = self.sensors
                if self.instance == 'JWST':
                    self.proxOperators.append('Project_on_JWST_diagonal')
                    self.proxOperators.append('Project_on_JWST_spheres')
                elif self.instance == 'CDP':
                    self.proxOperators.append('Project_on_CDP_diagonal')
                    self.proxOperators.append('Project_on_CDP_spheres')
                elif self.instance == 'Goettingen':
                    # not fully implemented
                    self.proxOperators.append('Project_on_shifted_diagonal')
                    self.proxOperators.append('Project_on_phase_spheres')
                else:
                    self.proxOperators.append('P_diag')
                    self.proxOperators.append('Project_on_spheres')

                u0 = Cell(self.sensors)
                for j in range(self.sensors):
                    u0[j] = self.u0

                self.u0 = Cell(3)
                self.u0[0] = u0[0]
                prox2_class = getattr(proxoperators, self.proxOperators[1])
                prox2 = prox2_class(self)
                self.u0[2] = prox2.eval(u0 - self.shift_data)
                prox1_class = getattr(proxoperators, self.proxOperators[0])
                prox1 = prox1_class(self)
                u0 = prox1.eval(self.u0[2] + self.shift_data)
                self.u0[1] = u0[0]
            else:
                errMsg = "formulation " + self.formulation \
                        + " for algorithm ' + + ' is not supported"
                raise NotImplementedError(errMsg)
        else:    
            errMsg = "formulation " + self.formulation \
                      + " is not supported"
            raise NotImplementedError(errMsg)



    def show(self):

        stats = self.output['stats']
        changes = stats['changes']
        changesSubplot = 111
        rel_errors = None
        if 'rel_errors' in stats:
            rel_errors = stats['rel_errors']
            changesSubplot = 211
        time = self.output['stats']['time']
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        
        iterations = np.arange(1, len(changes)+1, 1)
        algo_desc = self.algorithm.getDescription()
        algo_label = "Algorithm " + algo_desc + " (time = " + time_str + " s)"
        title = algo_label
        fig = plt.figure(figsize = (self.figure_width, self.figure_height),
                   dpi = self.figure_dpi)
        fig.suptitle(title)

        ax1 = fig.add_subplot(changesSubplot)
        ax1.semilogy(iterations, changes)
        ax1.set_xlabel('Iteration')
        yLabel = r'$|\!| x^{k+1}-x^{k} |\!|$'
        ax1.set_ylabel(yLabel)
        ax1.set_title("Change in iterates")

        if rel_errors is not None:
            ax2 = fig.add_subplot(212)
            ax2.semilogy(iterations, rel_errors)
            ax2.set_xlabel('Iteration')
            # yLabel = r'$|\!| P_1(x^{k})-P_2(x^{k}) |\!|$' # This is what Matlab does but this not right
            yLabel = 'Relative error'
            ax2.set_ylabel(yLabel)
            ax2.set_title("Relative errors")

        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height) between subplots (default = 0.2)
        plt.show()


