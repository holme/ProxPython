import json
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy
import time
from datetime import datetime
from math import sqrt
from numpy import square, mod
from numpy.linalg import norm

from proxtoolbox import algorithms
from proxtoolbox import proxoperators


class ExperimentMetaClass(type):
    """
    Metaclass associated with the Experiment class
    """

    def __call__(cls, *args, **kwargs):
        """
        Called when any instance of Experiment class is created.
        Does a few things:
        - set the experiment parameters based on the given keyword
        arguments and the default parameters given by the static method
        getDefaultParameters()
        - create the instance of the experiment class
        - call the initialize() method on the newly created instance
        (to load the data, set-up the prox operators and the algorithm).
        """
        actualArgs = cls.getDefaultParameters()  # retrieve default arguments (specified by derived class)
        actualArgs.update(kwargs)  # update these arguments based the on caller's arguments
        obj = type.__call__(cls, *args, **actualArgs)  # instantiate experiment object
        obj.initialize()  # initialize experiment object
        return obj


class JSONNumpyEncoder(json.JSONEncoder):
    """ json encoder for numpy types """

    def default(self, obj):  # pylint: disable=E0202
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, complex):
            return [obj.real, obj.imag]
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class Experiment(metaclass=ExperimentMetaClass):
    """
    Base class for ProxToolbox Experiments

    The experiment class contains all the information
    that describes a given experiment.

    Essentially, it loads or generates the data used in the
    experiment and instanciates the algorithm that works on
    this data. This class is meant to be derived from to implement
    concrete experiments. Derived classes must implement
    the static method getDefaultParameters() which returns a
    dictionary containing the default parameters used by this
    experiment. Also required, is the implementation of the
    loadData() method to load or generate the data.
    """

    def __init__(self,
                 experiment_name=None,
                 constraint='amplitude only',
                 object='complex',
                 formulation='product space',
                 product_space_dimension=1,
                 sets=None,
                 Nx=None,
                 Ny=None,
                 Nz=None,
                 noise=False,
                 algorithm=None,
                 numruns=1,
                 MAXIT=1000,
                 TOL=1e-8,
                 TOL2=None,
                 lambda_0=0.85,  # Initial relaxation value for first iterations
                 lambda_max=0.85,  # Final relaxation value for later iterations
                 lambda_switch=20,  # Timescale over which to change relaxation parameter
                 data_ball=1e-3,
                 diagnostic=True,
                 iterate_monitor_name='IterateMonitor',
                 accelerator_name=None,
                 rotate=False,
                 verbose=0,
                 silent=False, # if set to True, means no print statement should be executed
                               # (including what would have been generated with verbose mode)
                 graphics=0,
                 anim=False,
                 anim_step=2,  # Tell how frequently the animation is updated, i.e., every `anim_step` step(s)
                 anim_colorbar=False,  # if True the animated image will include a colorbar (updated at each iteration)
                 graphics_display=None,
                 figure_width=9,  # figure width in inches
                 figure_height=7,  # figure height in inches
                 figure_dpi=100,  # figure dpi for display
                 debug=False,
                 save_output=False,
                 json_output=True,
                 matlab_output=True,
                 rnd_seed=1234,  # Set to None to disable fixed pseudo-random number generator

                 **kwargs):
        """
        Initialize the data members of the experiment object. The real work
        will be done later in the initialize() method. The latter is called 
        automatically after the experiment object is fully constructed.
        """
        self.experiment_name = experiment_name

        self.constraint = constraint
        self.object = object
        self.formulation = formulation

        self.output = {}

        # Those data members will define the final dimensions
        # of the dataset to be used in this experiment.
        # At this stage we set them to the desired values
        # They may be changed by loadData() and/or ReshapeData()
        self.product_space_dimension = product_space_dimension
        self.sets = sets
        self.Nx = Nx
        self.Ny = Ny
        if Nz is not None:
            self.Nz = Nz
        else:  # TODO: Bad for experiments with 3D data, where Nz may be None to simply follow the data
            self.Nz = 1
        # we store here the original values specified by the user 
        self.desired_product_space_dimension = product_space_dimension
        self.desired_sets = sets
        self.desired_Nx = Nx
        self.desired_Ny = Ny
        self.desired_Nz = Nz

        self.noise = noise
        self.data_ball = data_ball
        self.diagnostic = diagnostic
        self.verbose = verbose
        self.silent = silent
        self.graphics = graphics
        self.anim = anim
        self.anim_step = anim_step
        self.anim_colorbar = anim_colorbar
        self.graphics_display = graphics_display
        self.figure_width = figure_width
        self.figure_height = figure_height
        self.figure_dpi = figure_dpi

        self.algorithm_name = algorithm  # name of algorithm to be used in experiment
        self.algorithm = None  # actual instance of algorithm to be used in experiment
        self.numruns = numruns
        self.MAXIT = MAXIT
        self.TOL = TOL
        self.TOL2 = TOL2
        self.lambda_0 = lambda_0
        self.lambda_max = lambda_max
        self.lambda_switch = lambda_switch

        self.save_output = save_output
        self.json_output = json_output
        self.matlab_output = matlab_output

        self.iterate_monitor_name = iterate_monitor_name
        self.accelerator_name = accelerator_name

        self.rotate = rotate  # tells the iterate monitor to rotate the iterates
        # to a fixed global reference
        self.rnd_seed = rnd_seed

        # additional attributes

        self.data = None
        self.data_sq = None
        self.norm_data = None
        self.norm_data_sq = None
        self.rt_data = None
        self.norm_rt_data = None
        self.u0 = None
        self.truth = None
        self.truth_dim = None
        self.norm_truth = None

        self.optimality_monitor = None

        self.proj_iter = None  # not sure what it does

        self.proxOperators = []
        self.nProx = None  # is the number of prox operators?

        self.productProxOperators = []
        self.n_product_Prox = None  # size of the product space
        # TODO: which default: None, 0 or 1 ?
        # see how it is used
        self.propagator = None
        self.inverse_propagator = None

        self.amplitude = None  # TODO Quick fix for now. Needed for P_amp prox operator

        self.run_number = 1

        # for animation:
        self.anim_figure = None

        self.debug = debug

    def initialize(self):
        """
        Initialize experiment object. This method is automatically called by
        the metaclass (ExperimentMetaClass) once the experiment object is
        fully constructed. This method calls the method loadData() that
        loads or generates the data required for this experiment.
        Additionally, it finds the classes of the prox operators, and
        instantiate the iterate monitor and the algorithm used to run 
        this experiment.
        """

        np.random.seed(self.rnd_seed)  # set random generator seed

        # load data
        self.loadData()
        self.reshapeData(self.Nx, self.Ny, self.Nz)  # TODO need to revisit this
        # - arguments not needed in particular
        if self.TOL2 is None:
            self.TOL2 = 1e-20

        # define the prox operators to be used for this experiment (list their names)
        # default implementation chooses prox operators based on constraint
        self.setupProxOperators()

        # retrieve the classes corresponding to the prox operators' names given above
        self.retrieveProxOperatorClasses()

        # TODO in Cone_and_Sphere estimation of the parameters above is done differently (set_fattener)
        # New matlab code:
        # % regularize the sets, if requested
        # if (any(strcmp('set_fattening', fieldnames(input))))&&(input.set_fattening)
        #    input=set_fattener(input);
        # old Python code:
        # gap0 = self.estimateGap()
        # self.data_ball = self.data_ball * gap0
        # self.TOL2 = self.data_ball * 1e-15
        # -----------
        self.setFattening()

        # instantiate iterate monitor, accelerator and algorithm
        self.instanciateAlgorithm()

        self.animation = None  # TODO difference with anim???

    def run(self):
        """
        Run the algorithm associated with this experiment.
        The initial iterate is obtained from the loadData() method.
        """

        if not self.silent:
            print("Running " + self.algorithm_name + " on " + self.experiment_name + "...")
        if self.verbose and not self.silent:
            self.printInput()

        t = time.time()

        self.output = self.runAlgorithm(self.u0)  # TODO: append to dict? (in overwrite mode)

        self.output['stats']['time'] = time.time() - t

        if not self.silent:
            print("Took", self.output['stats']['iter'], "iterations and",
                self.output['stats']['time'], "seconds.")

        self.postprocess()

        if self.save_output:
            self.saveOutput()

        if self.verbose and not self.silent:
            self.printBenchmark()

        if self.anim and mpl.is_interactive():
            plt.ioff()  # turn off interactive mode

    def runAlgorithm(self, u):
        """
        Run the algorithm associated with this experiment.
        Called by the run() method. May be overriden to provide
        additional behaviour, such as running a warmup algorithm
        before the main algorithm.

        Parameters
        ----------
        u: ndarray or cell of ndarray objects
            Initial iterate.
        
        Returns
        -------
        dictionary containing the output of the algorithm. In 
        particular, it contains the last iterate and various
        statistics.
        """
        return self.algorithm.run(u)

    def loadData(self):
        """
        Load or generate the dataset that will be used for
        this experiment. Create the initial iterate.
        """
        raise NotImplementedError("This is just an abstract interface")

    def setupProxOperators(self):
        """
        Determine the prox operators to be used based on the given constraint.
        This method is called during the initialization process. Derived classes
        can override this method to specify their own prox operators. Only the
        names of the prox operators need to be provided. The actual objects
        will be instantiated later.
        """
        self.proxOperators = []
        if self.constraint == 'hybrid':
            self.proxOperators.append('P_cP')  # This will be problem specific
        elif self.constraint == 'support only':
            self.proxOperators.append('P_S')
        elif self.constraint == 'real and support':
            self.proxOperators.append('P_S_real')
        elif self.constraint == 'nonnegative and support':
            self.proxOperators.append('P_SP')
        elif self.constraint == 'amplitude only':
            self.proxOperators.append('P_amp_support')
        elif self.constraint == 'phase on support':
            self.proxOperators.append('P_Amod')
        elif self.constraint == 'minimum amplitude':
            self.proxOperators.append('P_min_amp')
        elif self.constraint == 'phaselift':
            self.proxOperators.append('P_mean_SP')
            self.proxOperators.append('P_PL_lowrank')
        elif self.constraint == 'phaselift2':
            self.proxOperators.append('P_liftM')
            self.proxOperators.append(None)
            self.proxOperators.append('Approx_PM_Poisson')  # Patrick: This is just to monitor the change of phases!

    def retrieveProxOperatorClasses(self):
        """
        Retrieve the Python classes corresponding to the prox operators
        defined by the setupProxOperators() method.
        
        This is a helper method called during the initialization
        process. It should not be overriden, unless the derived 
        experiment class uses an additional category of prox operators
        not covered in the Experiment class.
        """

        # find prox operators classes based on their name
        # and replace names by actual classes
        proxOperatorClasses = []
        for prox in self.proxOperators:
            if prox != '':
                proxOperatorClasses.append(getattr(proxoperators, prox))
        self.proxOperators = proxOperatorClasses
        # same with product prox operators
        proxOperatorClasses = []
        for prox in self.productProxOperators:
            if prox != '':
                proxOperatorClasses.append(getattr(proxoperators, prox))
        self.productProxOperators = proxOperatorClasses

        # and propagator and inverse propagator
        if self.propagator is not None:
            self.propagator = getattr(proxoperators, self.propagator)
        if self.inverse_propagator is not None:
            self.inverse_propagator = getattr(proxoperators, self.inverse_propagator)

    def reshapeData(self, Nx, Ny, Nz):
        """
        Reshape data based on the given arguments. This method is called
        during the initialization of the Experiment object, after the call
        to loadData(). This optional. Default implementation does nothing.
        
        Parameters
        ----------
        Nx, Ny, Nz: int
            The new dimensions to be used. 

        Notes
        -----
        TODO: need to revisit this - there shouldn't be any need for arguments
        """
        pass

    def instanciateAlgorithm(self):
        """
        Instanciate algorithm and some objects used by the
        algorithm, namely the iterate monitor and accelerator,
        based on the names provided.

        This is a helper method called during the initialization
        process. It should not be overriden.
        """
        algorithmClass = getattr(algorithms, self.algorithm_name)
        iterateMonitorClass = getattr(algorithms, self.iterate_monitor_name)
        iterateMonitor = iterateMonitorClass(self)
        if self.accelerator_name is not None:
            acceleratorClass = getattr(algorithms, self.accelerator_name)
            accelerator = acceleratorClass(self)
        else:
            accelerator = None
        self.algorithm = algorithmClass(self, iterateMonitor, accelerator)

    def postprocess(self):
        """
        Optional processing of the data returned by the algorithm. Default
        implementation does nothing. May be overriden by derived classes.
        """
        pass

    def saveOutput(self):
        """
        Save the algorithm output. if the data member `matlab_output`
        is set to True, then the ouput is saved as a Matlab .mat file.
        If the data member `json_output` is set to True, then the
        output is saved using the json format.
        The output file is saved in a subdirectory of the OutputData
        folder. The name of the subdirectory is generated using
        the current date. The filename is obtained by concatenating
        the name of the experiment and the name of the algorithm used.
        """

        # Create directory (if needed)
        # does not raise an exception
        # if the directory already exists
        date_time = datetime.now()  # current date and time
        date_str = date_time.strftime("%Y_%m_%d")
        directory = '../OutputData/' + date_str + '/'
        os.makedirs(directory, exist_ok=True)

        filename_root = self.experiment_name + '_' + self.algorithm_name + '.output'
        file_path = directory + filename_root

        # prepare a special version of the output before saving it
        output = {}
        for key in self.output:
            if key == 'u1' or key == 'u2':
                pass
            elif key == 'u_monitor':
                # replace list with array
                length = len(self.output[key])
                u_mon = np.zeros(length, dtype=np.object)
                for i in range(length):
                    u_mon[i] = self.output[key][i]
                output[key] = u_mon
            else:
                output[key] = self.output[key]

        if self.matlab_output:
            # save .mat file
            file_path_ext = file_path + '.mat'
            # wrap all the variables in a dictionary ('python_output') as
            # in ProxMatlab, give it a different name to avoid name collision
            # with ProxMatlab
            scipy.io.savemat(file_path_ext, {'python_output': output})

        if self.json_output:
            # save json file
            file_path_ext = file_path + '.json'
            try:
                with open(file_path_ext, 'w') as file:
                    json.dump(output, file, indent=4, cls=JSONNumpyEncoder)
            except IOError as e:
                errMsg = e.__str__()
                print("Unable to write json output file ", file_path_ext, " : ", errMsg)
            except TypeError as e:
                errMsg = e.__str__()
                print("Unable to serialize object for json output file: ", errMsg)

    def show(self):
        """
        Generate graphical output from the solution
        Default implementation does nothing.
        """
        pass

    def animate(self, alg):
        """
        Display animation. This method is called
        by the iterate monitor if the data member `anim`
        is set to True. 

        This method is meant to be overriden in derived classes.
        In particular,the class PhaseExperiment provides an implementation
        which is used by all phase experiments.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """
        pass

    def printInput(self):
        """
        Print all the data members of this experiment
        """
        print("Input:")
        vars_dict = vars(self)
        for var in vars_dict:
            obj = vars_dict[var]
            # Only print variables that are not an instance of a class
            if hasattr(obj, '__dict__') is False:
                print('\t', var, ':', obj)

    def printBenchmark(self):
        """
        Create and print a benchmark, which is
        a summary the statistics collected 
        during the algorithm run.
        """
        benchmark = self.createBenchmark()
        print("Benchmark:")
        for var in benchmark:
            obj = benchmark[var]
            print('\t', var, ':', obj)

    def createBenchmark(self):
        """
        Create a summary of the statistics
        collected during the algorithm run.
        
        Returns
        -------
        benchmark: dictionary
            The benchmark.
        """
        benchmark = {}
        iterCount = self.output['stats']['iter']
        benchmark['iter'] = iterCount
        benchmark['time'] = self.output['stats']['time']
        changes = self.output['stats']['changes']
        benchmark['end_change'] = changes[iterCount - 1]
        if iterCount > 8:
            # if algorithm exits in fewer than 8 iterations, this will
            # throw an error because it is taking an average over
            # the last 10 iterations. Adjust accordingly.
            rates = changes[1:iterCount] / changes[0:iterCount - 1]
            rate = sum(rates[iterCount - 5:iterCount - 1]) / 3
            benchmark['rate'] = rate
            a_posteriori_error = rate / (1 - rate) * changes[iterCount - 2]
            benchmark['a_posteriori_error'] = a_posteriori_error
        if self.diagnostic:
            if self.truth is not None and 'rel_errors' in self.output['stats']:
                rel_errors = self.output['stats']['rel_errors']
                if iterCount == 1:
                    true_error = rel_errors[iterCount]
                else:
                    true_error = rel_errors[iterCount - 1]
                benchmark['true_error'] = true_error
            return benchmark

    def setFattening(self):
        """
        Optional method for fattening/regularizing sets
        Called by initialization() method after 
        instanciating prox operators but before instanciating
        the algorithm
        """
        pass

    def estimateGap(self):
        """
        Estimate the gap in the sequential formulation. 
        TODO: deprecated in Cone_and_Sphere
        """
        u1 = self.u0.copy()
        u2 = self.u0.copy()
        proj1 = self.proxOperators[0](self)
        if self.formulation == 'sequential':
            for j in range(self.product_space_dimension):
                self.proj_iter = j  # TODO this should be passed to the prox operator
                proj1 = self.proxOperators[0](self)
                u1[:, :, j] = proj1.eval(self.u0)
                self.proj_iter = mod(j, self.product_space_dimension) + 1
                proj1 = self.proxOperators[0](self)  # TODO recreate prox to update proj_iter
                u1[:, :, j] = proj1.eval(self.u0)
        else:  # i.e. formulation=='product space'
            proj1 = self.proxOperators[0](self)
            u1 = proj1.eval(self.u0)
            proj2 = self.proxOperators[1](self)
            u2 = proj2.eval(u1)

        # estimate the gap in the relevant metric
        if self.Nx == 1 or self.Ny == 1:
            tmp_gap = square(norm(u1 - u2) / self.norm_rt_data)
        elif self.product_space_dimension == 1:
            tmp_gap = (norm(u1 - u2) / self.norm_rt_data) ** 2
        else:
            tmp_gap = 0
            for j in range(self.product_space_dimension):
                # compute (||P_Sx-P_Mx||/norm_data)^2:
                tmp_gap = tmp_gap + (norm(u1[:, :, j] - u2[:, :, j]) / self.norm_rt_data) ** 2

        gap_0 = sqrt(tmp_gap)
        return gap_0

    def animateFigure(self, image, title):
        """
        Display animated figure. This is a helper method 
        that is called by the animate() method to display
        an animated image (drawn using imshow()). Other plots
        are not supported yet.

        This method takes care of creating or updating the
        figure
 
        Parameters
        ----------
        image : 2D scalar array
            The image to draw
        title : str
            The title of the figure
        """
        if self.anim_figure == None:
            # create figure
            plt.ion()
            self.anim_figure = plt.figure(figsize=(self.figure_width, self.figure_height),
                                          dpi=self.figure_dpi)
            self.anim_figure.canvas.mpl_connect('close_event', self.onAnimationWindowClosing)
            self.anim_ax = self.anim_figure.add_subplot(111)
            self.anim_bar = None
            self.anim_im = self.anim_ax.imshow(image)
            self.anim_ax.set_title(title)
            self.anim_figure.canvas.draw()
            # plt.show(block=False)
        else:
            self.anim_im.set_array(image)
            min_val = np.min(image)
            max_val = np.max(image)
            self.anim_im.set_clim(vmin=min_val, vmax=max_val)
            if self.anim_colorbar:
                if self.anim_bar is not None:
                    self.anim_bar.remove()
                self.anim_bar = plt.colorbar(self.anim_im, ax=self.anim_ax, fraction=0.046, pad=0.04)
            self.anim_ax.set_title(title)
            self.anim_ax.draw_artist(self.anim_ax.patch)
            self.anim_ax.draw_artist(self.anim_im)
            self.anim_figure.canvas.draw_idle()
            self.anim_figure.canvas.flush_events()

    def onAnimationWindowClosing(self, evt):
        self.anim_figure = None

    @staticmethod
    def getDefaultParameters():
        """
        Return the default parameters used by this experiment. The name
        of the parameters must match the arguments of the __init__() 
        methods of the Experiment class and/or derived classes.
        Essentially this corresponds to the old config files. 
        This static method must be implemented in the derived classes (the
        implementation given here only throws an exception).
        See example below.
        
        Returns
        -------
        dictionary
            Default parameters to be used for this experiment. Each entry
            must match an argument from the __init__() methods 
            of the Experiment class and/or derived classes.

        Notes
        -------
        The following shows a typical implementation of this method:

        @staticmethod
        def getDefaultParameters():
            defaultParams = {
                'object': 'complex',
                'constraint': 'amplitude only',
                'distance': 'far field',
                'Nx': 128,
                'Ny': 128,
                'Nz': 1,
                'product_space_dimension': 4,
                'MAXIT': 6000,
                'TOL': 5e-5,
                'lambda_0': 0.85,
                'lambda_max': 0.85,
                'lambda_switch': 20,
                'data_ball': 1e-30,
                'diagnostic': True,
                'iterate_monitor_name': 'FeasibilityIterateMonitor',
                'verbose': 1,
                'graphics': 1,
                'anim': False
            }
            return defaultParams

        """
        raise NotImplementedError("Static method getDefaultParameters() must \
                                  be implemented in any concrete class derived from \
                                  Experiment class")
