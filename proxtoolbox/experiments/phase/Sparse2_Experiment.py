
from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox import proxoperators
from proxtoolbox.utils.loadMatFile import loadMatFile
import numpy as np
from numpy import fromfile, exp, nonzero, zeros, pi, resize, real, angle
from numpy.random import rand
from numpy.linalg import norm
from numpy.fft import fftshift, fft2, ifft2
from math import sqrt, ceil

import proxtoolbox.utils as utils
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.gaussian import gaussian

#for downloading data
import proxtoolbox.utils.GetData as GetData
from proxtoolbox.utils.GetData import datadir

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class Sparse2_Experiment(PhaseExperiment):
    '''
    Sparse2 experiment class
    '''

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name' : 'Sparse2',
            'object': 'nonnegative',
            'constraint': 'sparse nonnegative',
            'distance': 'far field',
            'Nx': 128,
            'Ny': 128,
            'Nz': 1,
            'noise': False,
            'snr': 20,
            'Sparsity': .003,
            'cutoff': 10,
            'MAXIT': 1000,
            'TOL': 1e-4,
            'lambda_0': 0.85,
            'lambda_max': 0.85,
            'lambda_switch': 20,
            'data_ball': 1e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'verbose': 0,
            'graphics': 1
        }
        return defaultParams
    

    def __init__(self, snr=20, Sparsity=.003, cutoff=10, **kwargs):
        super(Sparse2_Experiment, self).__init__(**kwargs)
        self.snr = snr
        self.Sparsity = Sparsity # Sparsity is the PERCENTAGE of nonzero elements
        self.cutoff = cutoff

    def loadData(self):
        """
        Load Sparse2 dataset. Create the initial iterate.

        Notes
        -----
        This experiment seems to be very sensitive to numerical precision.
        The consequence is that the number of iterations needed to solve
        the problem will vary compared to Matlab. This is due to the fact
        that Python and Matlab does not use the exact same algorithms for
        computing fft2, iff2.

        """
        #make sure input data can be found, otherwise download it
        GetData.getData('Phase')

        n = self.Nx * self.Ny
        rt_n = sqrt(n)
        Sparsity = self.Sparsity
        tmp = np.random.rand(n)
        tmp2 = np.sort(tmp)[::-1] # descend sort
        I = np.argsort(tmp)[::-1] 
        support = np.zeros(n)
        if self.cutoff is None:
            cutoff = 15
        else:
            cutoff = self.cutoff
        support[I[0:cutoff]] = np.ceil(tmp2[0:cutoff])

        self.truth = np.transpose(np.reshape(support*(100*rand(n)), (self.Ny, self.Nx)))
        self.truth_dim = self.truth.shape
        support_idx = np.where(support != 0)[0]
        tmp_shifted = np.roll(support_idx, -1)
        tmp_shifted[len(tmp_shifted)-1] = support_idx[0]+n
        self.truth_supp_diff = tmp_shifted - support_idx

        g = gaussian(self.Ny, [Sparsity], [ceil(rt_n/2), ceil(rt_n/2)])
        self.truth = np.real(fftshift(ifft2(fft2(g)*fft2(self.truth))))
        flat_truth = self.truth.flatten('F')
        u_scale = np.min(flat_truth[support_idx])
        self.truth = self.truth / u_scale
        support_idx = np.where(self.truth < 1)
        self.truth[support_idx] = 0
        self.norm_truth = norm(self.truth, ord=2)
        self.true_support = self.truth > 0
        tmp_true_supp = np.zeros_like(self.truth)
        tmp_true_supp[self.true_support] = 1
        self.cov_true_support = fftshift(np.real(ifft2(fft2(tmp_true_supp)*ifft2(tmp_true_supp)))/self.Nx)
        tmp_supp_index = np.zeros_like(self.truth)
        tmp_supp_index[support_idx] = 1
        self.true_sparsity = n - np.sum(np.sum(tmp_supp_index))
        self.sparsity_parameter = np.ceil(1.2*self.true_sparsity)
        
        M = (np.abs(fft2(self.truth)))**2/n   # note: modulus squared, not modulus
        
        if self.noise:
            snr = self.snr
            tmp2 = np.zeros(M.size())
            tmp3 = tmp2.copy()
            for i in range(n):
                r = max(abs(M[i]), 1e-10)
                tmp2[i] = utils.PoissonRan(r/snr)
                tmp3[i] = M[i] + tmp2[i]*M[i]/r
            M = tmp3

        self.rt_data = Cell(1)
        self.rt_data[0] = M**(0.5)
        # standard for the main program is that
        # the data field is the magnitude SQUARED
        # in Luke.m this is changed to the magnitude.
        self.data = Cell(1)
        self.data[0] = M
        self.norm_rt_data = norm(self.rt_data[0], ord='fro')

        self.norm_data = self.norm_rt_data**2
        self.data_zeros = Cell(1)
        self.data_zeros[0] = np.where(M==0)
        self.sets = 2
        
        # initial guess
        self.u0 = ifft2(self.rt_data[0] + 10*(np.random.rand(self.Nx, self.Ny)).T)



    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(Sparse2_Experiment, self).setupProxOperators()  # call parent's method

        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

        self.proxOperators.append('P_nonneg_sparsity')
        self.proxOperators.append('Approx_Pphase_FreFra_Poisson')
        self.nProx = 2

