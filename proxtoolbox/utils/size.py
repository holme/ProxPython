
import numpy as np

def size_matlab(u):
    '''
    This utility function mimics what the Matlab function size does.
    Works with multidimensional arrays and returns the length of the
    data corresponding to the first 4 dimensions. It returns 1 when
    the dimension does not exist. Although this does not make a lot
    of sense in Python, this is done here to insure compatibility
    with Matlab and previous Python code.

    Parameters
    ----------
    u : array_like

    Returns
    -------
    n, m, p, q: tuple of integers
        Size of each of the first 4 dimensions of 'u'. Returns 1 when
        dimension does not exist (for compatibility with Matlab)
    '''
    m = u.shape[0]
    n = 1
    p = 1
    q = 1
    if u.ndim > 1:
        n = u.shape[1]
    if u.ndim > 2:
        p = u.shape[2]
    if u.ndim > 3:
        q = u.shape[3]
    return m,n,p,q
     


