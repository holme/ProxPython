"""              MgenN.py
            written on 9. Sep 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: < Description >
    Input: rows, k; columns, n; variance, v; mean, m; seed. seed.

    Output: Random matrix with normally distributed (mean m, variance v) iid elements.
    Usage: rand_output = MgenN(k, n, v, m, seed) """
    
    
    
#from random import seed as ran_seed
from numpy.random import seed as ran_seed, randn
#from numpy import ones
 
def MgenN(k, n, v, m, seed):
    # rand('state',sum(100*clock))
    ran_seed(seed)              # set the seed for the random generator to "seed"
                                    # not quite like rand('state', seed); in the .m file
                                    
    return v * randn(k,n) + m
    
    
    
if __name__ == "__main__":
    print(MgenN(2,2,1,0,3036))