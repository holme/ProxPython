import numpy as np
from matplotlib.colors import hsv_to_rgb


def complex_to_hsv(arr: np.ndarray, background: str = 'dark'):
    """
    Transforms a 2d complex-valued field to an HSV image, with hue being related to phase and
    saturation or value with the amplitude of the field

    Args:
        arr: numpy array with complex dtype
        background: 'dark' or 'light'

    Returns:

    """
    norm = np.max([np.max(abs(arr)), 1e-15])
    objecthsv = np.zeros((3,) + arr.shape)
    objecthsv[0] = np.angle(-arr) / 2 / np.pi + 0.5
    if background == 'dark':
        objecthsv[1] = np.ones(arr.shape)
        objecthsv[2] = abs(arr) / norm
    else:
        objecthsv[2] = np.ones(arr.shape)
        objecthsv[1] = abs(arr) / norm
    return np.transpose(objecthsv, (1, 2, 0))


def complex_to_rgb(arr, background='dark'):
    """
    Transforms a 2d complex-valued field to an RGB image, with color being related to phase and
    brightness with the amplitude of the field

    Args:
        arr: numpy array with complex dtype
        background: 'dark' or 'light'

    Returns:

    """
    return hsv_to_rgb(complex_to_hsv(arr, background=background))
