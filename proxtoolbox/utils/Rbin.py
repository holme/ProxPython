"""              Rbin.py
            written on Sep 2. 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: .npz-file or .npz for reading binary files into python.
    Input: 'filename' must specify the entire directory path
            'Type' is either "npy" or "npz" 


    Output: reads data from 'filename'
    Usage: data=Rbin('filename','Type') """
    

    
    
    
from numpy import load



def Rbin(file, Type):
    
    if Type == "npy":
        A = load(file)
        
        return A

    else:
        data = load(file)
        return data
    
        