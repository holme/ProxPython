
from proxtoolbox.algorithms.iterateMonitor import IterateMonitor
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import zeros, angle, trace, exp, sqrt, sum, matmul, array, reshape
from numpy.linalg import norm

class PHeBIE_IterateMonitor(IterateMonitor):
    """
    Algorithm analyzer for monitoring iterates of 
    projection algorithms for the PHeBIE algorithm. 
    Specialization of the IterateMonitor class.
    """

    def __init__(self, experiment):
        super(PHeBIE_IterateMonitor, self).__init__(experiment)
        self.gaps = None
        self.rel_errors = None
        self.product_space_dimension = experiment.product_space_dimension
        self.Nz = experiment.Nz


    def preprocess(self, alg):
        """
        Allocate data structures needed to collect 
        statistics. Called before running the algorithm.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm to be monitored.
        """
        super(PHeBIE_IterateMonitor, self).preprocess(alg)

        # In PHeBIE, the iterate u is a cell of blocks of variables.
        # In the analysis of Hesse, Luke, Sabach&Tam, SIAM J. Imaging Sciences, 2015
        # all blocks are monitored for convergence.
        self.u_monitor = self.u0.copy()

        # set up diagnostic arrays
        if self.diagnostic:
            self.gaps = self.changes.copy()
            self.gaps[0] = 999
            if self.truth is not None:
                self.rel_errors = self.changes.copy()
                self.rel_errors[0] = sqrt(999)
           
    def updateStatistics(self, alg):
        """
        Update statistics. Called at each iteration
        while the algorithm is running.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """
        u = alg.u_new
        prev_u = self.u_monitor
        normM = self.norm_data
        nProx = len(alg.proxOperators)
        tmp_change = 0
        for j in range(nProx):
            if isCell(u[j]):
                for jj in range(len(u[j])):
                    tmp_change += (norm(u[j][jj] - prev_u[j][jj], 'fro')/normM)**2
            else:
                p, q = self.getIterateSize(u[j])
                if p == 1 and q == 1:
                    tmp_change += (norm(u[j] - prev_u[j], 'fro')/normM)**2
                elif q == 1:
                    for jj in range(self.product_space_dimension):
                        tmp_change += (norm(u[j][:,:,jj] - prev_u[j][:,:,jj], 'fro')/normM)**2
                else: # cells of 4D arrays?!!!
                    for jj in range(self.product_space_dimension):
                        for k in range(self.Nz):
                            tmp_change += (norm(u[j][:,:,k,jj] - prev_u[j][:,:,k,jj], 'fro')/normM)**2
                
        self.changes[alg.iter] = sqrt(tmp_change)
        if self.diagnostic:
            self.gaps[alg.iter] = self.calculateObjective(alg)
            if self.truth is not None:
                rel_error = norm(self.truth - exp(-1j*angle(trace(matmul(self.truth.T.conj(), u[0])))) * u[0],'fro') / self.norm_truth
                self.rel_errors[alg.iter] = rel_error
        
        self.u_monitor = u.copy()


    def postprocess(self, alg, output):
        """
        Called after the algorithm stops. Store statistics in
        the given 'output' dictionary

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
        output : dictionary
            Contains the last iterate and various statistics that
            were collected while the algorithm was running.
                
        Returns
        -------
        output : dictionary into which the following entries are added
            (when diagnostics are required)
        gaps : ndarray
            Squared gap distance normalized by the magnitude
            constraint
        """
        output = super(PHeBIE_IterateMonitor, self).postprocess(alg, output)
        if self.diagnostic:
            stats = output['stats']
            stats['gaps'] = self.gaps[1:alg.iter+1]
            if self.truth is not None:
                stats['rel_errors'] = self.rel_errors[1:alg.iter+1]
        return output