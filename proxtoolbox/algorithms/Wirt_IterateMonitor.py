
from proxtoolbox.algorithms.iterateMonitor import IterateMonitor
from proxtoolbox.utils.cell import Cell, isCell
import numpy as np
from numpy import zeros, angle, trace, exp, sqrt, sum, matmul, array, reshape
from numpy.linalg import norm
import time

class Wirt_IterateMonitor(IterateMonitor):
    """
    Algorithm analyzer for monitoring iterates of 
    projection algorithms for the Wirtinger algorithm. 
    Specialization of the IterateMonitor class.
    """

    def __init__(self, experiment):
        super(Wirt_IterateMonitor, self).__init__(experiment)
        self.gaps = None
        self.rel_errors = None
        self.times = None
        self.product_space_dimension = experiment.product_space_dimension
        self.Nz = experiment.Nz
        self.startTime = 0


    def preprocess(self, alg):
        """
        Allocate data structures needed to collect 
        statistics. Called before running the algorithm.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm to be monitored.
        """
        super(Wirt_IterateMonitor, self).preprocess(alg)

        # In PHeBIE, the iterate u is a cell of blocks of variables.
        # In the analysis of Hesse, Luke, Sabach&Tam, SIAM J. Imaging Sciences, 2015
        # all blocks are monitored for convergence.
        self.u_monitor = self.u0.copy()

        # set up diagnostic arrays
        if self.diagnostic:
            self.gaps = self.changes.copy()
            self.gaps[0] = 999
            if self.truth is not None:
                self.rel_errors = self.changes.copy()
                self.rel_errors[0] = sqrt(999)
                self.times = self.changes.copy()
                self.times[0] = 0
        
        self.startTime = time.time()

           
    def updateStatistics(self, alg):
        """
        Update statistics. Called at each iteration
        while the algorithm is running.
 
        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm being monitored.
        """
        u = alg.u_new
        normM = self.norm_data
        tmp_change = 0
        tmp_gap = 0
        rel_error = 0
        if self.diagnostic:
            tmp_u = alg.prox1.eval(u)
            tmp1 = alg.prox2.eval(tmp_u)

        if isCell(u):
            for j in range(len(u)):
                tmp_change += (norm(alg.step[j])/normM)**2
                if self.diagnostic:
                    tmp_gap += (norm(tmp1[j] - tmp_u[j])/normM)**2
            if self.diagnostic and self.truth is not None:
                z = u[0]
                if self.truth_dim[0] == 1:
                    z = reshape(z, (1,len(z)))  # we want a true matrix not just a vector
                elif self.truth_dim[1] == 1:
                    z = reshape(z, (len(z),1))  # we want a true matrix not just a vector
        else:
            tmp_change = sum(abs(alg.step)**2)/normM**2
            if self.diagnostic:
                tmp_gap = sum(abs(tmp1 - tmp_u)**2)/normM**2
                if self.truth is not None:
                    if u.ndim == 1:
                        # u is a vector
                        z = u
                        z = reshape(z, (len(z),1))  # we want a true matrix not just a vector
                    else: # u is a multidimensional array
                        if self.truth_dim[0] == 1:
                            z = u[0,:]
                            z = reshape(z, (1,len(z)))  # we want a true matrix not just a vector
                        elif self.truth_dim[1] == 1:
                            z = u[:,0]
                            z = reshape(z, (len(z),1))  # we want a true matrix not just a vector
                        else:
                            z = u[:,:,0]
        if self.diagnostic and self.truth is not None:
            rel_error = norm(self.truth - exp(-1j*angle(trace(self.truth.T.conj() @ z))) * z) / self.norm_truth

        self.changes[alg.iter] = sqrt(tmp_change)
        if self.diagnostic:
            self.gaps[alg.iter] = sqrt(tmp_gap)
            if self.truth is not None:
                self.rel_errors[alg.iter] = rel_error
                self.times[alg.iter] = time.time() - self.startTime

        self.u_monitor = u.copy()


    def postprocess(self, alg, output):
        """
        Called after the algorithm stops. Store statistics in
        the given 'output' dictionary

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that was monitored.
        output : dictionary
            Contains the last iterate and various statistics that
            were collected while the algorithm was running.
                
        Returns
        -------
        output : dictionary into which the following entries are added
            (when diagnostics are required)
        gaps : ndarray
            Squared gap distance normalized by the magnitude
            constraint
        """
        output = super(Wirt_IterateMonitor, self).postprocess(alg, output)
        if self.diagnostic:
            stats = output['stats']
            stats['gaps'] = self.gaps[1:alg.iter+1]
            if self.truth is not None:
                stats['rel_errors'] = self.rel_errors[1:alg.iter+1]
                stats['times'] = self.times[1:alg.iter+1]
        return output