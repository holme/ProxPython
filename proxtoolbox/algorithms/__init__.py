
"""
This package contains the different algorithms that can 
be used when running the various experiments contained in
the proxtoolbox. It contains the abstract Algorithm class and
various concrete classes. 
"""

from .AP import *
from .DRAP import *
from .RAAR import *
from .RRR import *
from .HPR import *
from .CP import *
from .AvP import *
from .QNAvP import *
from .DRl import *
from .CDRl import *
from .CADRl import *
from .PHeBIE import *
from .iterateMonitor import *
from .feasibilityIterateMonitor import *
from .PHeBIE_IterateMonitor import*
from .CT_IterateMonitor import*
from .ADMM import *
from .ADMM_IterateMonitor import *
from .ADMM_PhaseLagrangeUpdate import *
from .ADMM_phase_indicator_objective import *
from .AvP2 import *
from .AvP2_IterateMonitor import *
from .GenericAccelerator import *
from .DyRePr import*
from .PHeBIE_ptychography_objective import *
from .PHeBIE_phase_objective import *
from .Wirtinger import *
from .Wirt_IterateMonitor import *
from .NSLS_sourceloc_objective import *