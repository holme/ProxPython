

Getting started
---------------
The easiest way to use the ProxToolbox is to run one of the
demonstrations that are located in the :file:`demos` folder.
Consider, for instance, the demonstration that uses
the Cyclic Projections (CP) algorithm on the CDI experiment.


.. contents::
   :local:


Running the CP algorithm on the CDI experiment
++++++++++++++++++++++++++++++++++++++++++++++

Assuming :file:`ProxPython` is in your current folder,
change your current folder to :file:`demos` by::
    
    cd ProxPython/demos

Then just type::
    
    python3 tasse_cp.py

This demonstration will produce the following text output:

.. code-block:: none

   Loading data file CDI_intensity
   Using farfield formula.
   Running CP on CDI...
   Took 667 iterations and 5.126296758651733 seconds.

It will also open a few windows to display several plots.
Closing all the windows will end this python script.

Now, let us examine the content of the :mod:`tasse_cp.py` demonstration:

.. code-block:: python
   :linenos:

   import SetProxPythonPath
   from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

   CDI = CDI_Experiment(algorithm='CP', MAXIT=5000)
   CDI.run()
   CDI.show()


The first line makes sure that the folder :file:`ProxPython` is in
the python path. The second line imports the :class:`CDI_Experiment`
class that we are going to use. Line 4 creates a :class:`CDI_Experiment`
object using two arguments. 
The :attr:`algorithm` argument specifies which algorithm to use, while 
the :attr:`MAXIT` argument gives the maximum number of iterations the
algorithm can run. The next line runs the algorithm on the CDI 
experiment and the last line displays the relevant plots for this experiment. 



Modifying this demonstration
++++++++++++++++++++++++++++

The :class:`CDI_Experiment` class, like every experiment in the ProxToolbox,
defines default parameters via its static method :meth:`getDefaultParameters`

.. code-block:: python
   :linenos:

   @staticmethod
   def getDefaultParameters():
      defaultParams = {
         'experiment_name': 'CDI',
         'object': 'nonnegative',
         'constraint': 'nonnegative and support',
         'Nx': 128,
         'Ny': 128,
         'Nz': 1,
         'sets': 10,
         'farfield': True,
         'MAXIT': 6000,
         'TOL': 1e-8,
         'lambda_0': 0.5,
         'lambda_max': 0.50,
         'lambda_switch': 30,
         'data_ball': .999826e-30,
         'diagnostic': True,
         'iterate_monitor_name': 'FeasibilityIterateMonitor',
         'rotate': False,
         'verbose': 0,
         'graphics': 1,
         'anim': False,
         'debug': True
      }
      return defaultParams

In the previous demonstration, 
the :attr:`MAXIT` argument was used during the creation of
the experiment object to override the default value of 
:data:`6000`. 

For additional practice, we can create an experiment object
that will use a different algorithm and specific
values for the parameters :attr:`lambda_0` and :attr:`lambda_max`
which govern how the relaxation constant is calculated:

.. code-block:: python
   :linenos:

   CDI = CDI_Experiment(algorithm='CDRl', lambda_0=0.7, lambda_max=0.9)



