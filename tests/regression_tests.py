import SetProxPythonPath
from proxtoolbox.experiments.CT.ART_Experiment import ART_Experiment
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment
from proxtoolbox.experiments.ptychography.ptychographyExperiment import PtychographyExperiment
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment
from proxtoolbox.experiments.phase.Sparse2_Experiment import Sparse2_Experiment
from proxtoolbox.experiments.sudoku.sudokuExperiment import SudokuExperiment
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment
from proxtoolbox.experiments.orbitaltomography import PlanarMolecule, DegenerateOrbital, OrthogonalOrbitals

import numpy as np
import os
import enum
import time

def getDict(**kwargs):
    return kwargs

def printValue(value):
    if isinstance(value, str):
        print("\'" + value + "\'", end='')
    else:
        print(value, end='')


class TestEngine:
    '''
    TestEngine class
    This class has three purposes:
        1. Generation of the data structure used for the regression tests. 
        2. Update of the data structure used for the regression tests
        3. Running the regression tests

    See script below to see how it is used.
    '''

    def __init__(self, testData=None, maxIterCount=5, maxError=1e-5, **kwargs):
        self.testData = testData
        self.maxIterCount = maxIterCount
        self.maxError = maxError


    def run(self):
        self.failedCount = 0
        for demo in self.testData:
            trueChange = demo['change']
            experiment = self.instantiateExperiment(demo)
            algorithmName = demo['args']['algorithm']
            print("Running " + algorithmName + " on " + experiment.experiment_name + "... ", end='')
            experiment.run()
            output = experiment.output
            changes = output['stats']['changes']
            actualChange = changes[len(changes)-1] # take last value
            # test 
            success, percentageDiff = self.checkValue(trueChange, actualChange)
            if success:
                print(" Passed")
            else:
                print(" Failed")
                print("\t with percentage difference of", percentageDiff*100, '%')
                self.failedCount += 1
        return (self.failedCount == 0)

    def generateTestData(self):
        self.testData = []
        directory = '../demos'
        for filename in os.listdir(directory):
            if filename.endswith(".py") and filename != "RunAll.py":
                path = directory + "/" + filename
                with open(path) as file:
                    content = file.read()
                    # look for "proxtoolbox.experiments"
                    index = content.find("proxtoolbox.experiments")
                    if index == -1:
                        print("Skipping %s at experiment check" % filename)
                        continue
                    content = content[index:]
                    # look for next import statement
                    index = content.find("import")
                    if index == -1:
                        print("Skipping %s as no import found" % filename)
                        continue
                    content = content.split("import", 1)[1]
                    # the next token is the name of the experiment class
                    tokens = content.split()
                    experimentClassName = tokens[0]
                    # for now skip Ptychography due to warmup_params argument
                    # that refer to another variable
                    # we skip Phasepack_Experiment because this consumes too much memory,
                    # and ART because it takes too much time to run those demos
                    if experimentClassName == "PtychographyExperiment" \
                         or experimentClassName == "Phasepack_Experiment" \
                         or experimentClassName == "ART_Experiment": \
                        continue
                    content = content.split(experimentClassName,1)[1]
                    # look for next occurence of experiment class
                    index = content.find(experimentClassName)
                    if index == -1:
                        print("Skipping %s as no classname found" %filename)
                        continue
                    content = content[index:]
                    # look for arguments
                    content = content.split("(",1)[1]
                    index = content.find(")")
                    args = content[:index]
                    # add what we found in testData
                    exp_dict = {}
                    exp_dict['experiment'] = experimentClassName
                    dict_args = eval('getDict(' + args + ')')
                    exp_dict['args'] = dict_args
                    print("processed", filename)
                    self.testData.append(exp_dict)

        self.printTestData()

    def updateTestData(self):
        for demo in self.testData:
            experiment = self.instantiateExperiment(demo)
            algorithmName = demo['args']['algorithm']
            print("Running " + algorithmName + " on " + experiment.experiment_name + "... ")
            experiment.run()
            output = experiment.output
            changes = output['stats']['changes']
            actualChange = changes[len(changes)-1] # take last value
            demo['change'] = actualChange
        self.printTestData()

    def instantiateExperiment(self, demo):
        # retrieve demo info
        experimentName = demo['experiment']
        args = demo['args']
        # retrieve class from experiment name. This works because of 
        # import statement at the beginning of file
        experimentClass = globals()[experimentName]
        # instantiate and run experiment
        MAXIT = self.maxIterCount
        if 'MAXIT' in demo:
            MAXIT = demo['MAXIT']
        args['MAXIT'] = MAXIT
        args['silent'] = True # we don't want any message during those runs
        experiment = experimentClass(**args)
        return experiment

    def checkValue(self, trueVal, actualVal):
        percentageDiff = np.abs((trueVal - actualVal) / (trueVal+1e-30))
        return (percentageDiff <= self.maxError), percentageDiff

    def printTestData(self):
        print("\ttestData = [")
        demoCount = len(self.testData)
        i = 1
        for demo in self.testData:
            print("\t\t{", "\'experiment\' :", "\'" + demo['experiment'] + "\'",
                  ',', "\n\t\t  \'args\' : getDict(", end='')
            args = demo['args']
            j = 0
            for arg, value in args.items():
                if arg == 'silent' or arg == 'MAXIT': # skip arguments that are set by this class
                    continue
                if j != 0:
                    print(', ', end='')
                if j > 3:
                    print("\n\t\t\t", end='')
                print(arg, "= ", end='')
                printValue(value)
                j += 1
            print(")", end='')
            # optional keys
            if 'change' in demo or 'MAXIT' in demo:
                print(",")
                if 'change' in demo:
                    print("\t\t  \'change\' :", demo['change'], end='')
                if 'MAXIT' in demo:
                    print(",")
                    print("\t\t  \'MAXIT\' :", demo['MAXIT'], end='')
            print(' }', end='')
            if i != demoCount: # this is not the last demo
                print(',')
            i += 1
        print("\n\t\t]")

class Mode(enum.Enum):
   GenerateTestData = 1
   UpdateTestData = 2
   RunTests = 3

if __name__ == "__main__":
    """
    These regression tests will run a list of demos 
    for a given number of iterations.
    
    There are three ways to use this script:

    1. Generation of the data structure used for the regression tests: This 
    data structure is created by parsing and extracting the relevant information
    from the demo files contained in the demos folder. The data structure
    is formally a list of dictionaries. Each dictionary describes a demo by specifying
    the experiment class and the arguments required to instantiate an experiment.
    The resulting data structure is printed in the terminal window used to run this script
    and must be copy-pasted into the script (as the 'testData' variable)
    Optionally, a `MAXIT` entry can be manually added to the dictionary describing each demo
    If present, this will override the default number of iterations that is used
    to run all the demos.

    2. Update of the data structure: the script runs all the demos described in
    the data structure and a `change` entry is added in the dictionary describing each demo.
    This value describes the change in iterates when the specified 
    number of iterations is reached.

    3. Running the regression tests: This is the most common use of this script.
    It requires that the data structure used for the tests exists and provides
    a `change` entry for each demo described. The script runs each demo and 
    the actual change in iterates is compared to the value contained in the data structure.

    """
    testData = [
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'ADMM', constraint = 'support only', lambda_0 = 10, lambda_max = 10,
                    warmup_iter = 50),
                'change' : 21.38973820888686 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, TOL = 5e-10),
                'change' : 0.07197489907000376 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, TOL = 5e-10,
                    warmup_iter = 50),
                'change' : 0.0632047418628188 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP'),
                'change' : 0.3733544093581861 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', warmup_iter = 50),
                'change' : 0.18271647829249518 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33),
                'change' : 0.007436934474180596 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33,
                    warmup_iter = 50),
                'change' : 2.353770515985933e-06 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0),
                'change' : 1.3483502765424795 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0,
                    warmup_iter = 50),
                'change' : 1.4086283818000096 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CP'),
                'change' : 0.6598811706693641 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CP', warmup_iter = 50),
                'change' : 0.280239534462918 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRAP'),
                'change' : 1.5940317693439379 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', warmup_iter = 50),
                'change' : 0.2158119842646279 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.6, lambda_max = 0.6),
                'change' : 0.7858468818207096 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.6, lambda_max = 0.6, warmup_iter = 50),
                'change' : 0.17889304676237733 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0),
                'change' : 3.4409733151930686 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, warmup_iter = 50),
                'change' : 0.583302391442773 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', constraint = 'support only'),
                'change' : 0.001298372736005032 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', constraint = 'support only', warmup_iter = 50),
                'change' : 0.0007221671483883771 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', constraint = 'support only', accelerator_name = 'GenericAccelerator'),
                'change' : 0.7138593217728066 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', constraint = 'support only', accelerator_name = 'GenericAccelerator', warmup_iter = 50),
                'change' : 0.2252889914846455 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 0.44803286573108864 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', warmup_iter = 50),
                'change' : 0.2670769841235402 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger'),
                'change' : 0.05786999221546579 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger', warmup_iter = 50),
                'change' : 0.07937133199674928 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', Nx = 256, lambda_0 = 0.75, lambda_max = 0.75,
                    TOL = 1e-08),
                'change' : 0.06751391565651677 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', Nx = 256, lambda_0 = 0.75, lambda_max = 0.75,
                    TOL = 1e-08,
                    warmup_iter = 50),
                'change' : 0.08602210480780262 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', Nx = 256, TOL = 1e-08),
                'change' : 0.3625156614661598 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', Nx = 256, TOL = 1e-08, warmup_iter = 50),
                'change' : 0.322530934435349 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', Nx = 256, formulation = 'cyclic', lambda_0 = 0.3,
                    lambda_max = 0.3,
                    TOL = 1e-08),
                'change' : 0.8348677561838059 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', Nx = 256, formulation = 'cyclic', lambda_0 = 0.3,
                    lambda_max = 0.3,
                    TOL = 1e-08,
                    warmup_iter = 50),
                'change' : 1.9398112808433364e-05 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', Nx = 256, formulation = 'cyclic', lambda_0 = 1.0,
                    lambda_max = 1.0,
                    TOL = 1e-08),
                'change' : 1.2763318469136187 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', Nx = 256, formulation = 'cyclic', lambda_0 = 1.0,
                    lambda_max = 1.0,
                    TOL = 1e-08,
                    warmup_iter = 50),
                'change' : 1.2799166544493183 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CP', Nx = 256, formulation = 'cyclic', lambda_0 = 0.75,
                    lambda_max = 0.75,
                    TOL = 1e-08,
                    data_ball = 1e-15,
                    iterate_monitor_name = 'IterateMonitor'),
                'change' : 1.5013580601600591 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'CP', Nx = 256, formulation = 'cyclic', lambda_0 = 0.75,
                    lambda_max = 0.75,
                    TOL = 1e-08,
                    data_ball = 1e-15,
                    warmup_iter = 50,
                    iterate_monitor_name = 'IterateMonitor'),
                'change' : 0.0024685905153934077 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', Nx = 256, TOL = 1e-08, data_ball = 1e-15,
                    iterate_monitor_name = 'IterateMonitor'),
                'change' : 1.7164795077647284 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', Nx = 256, TOL = 1e-08, data_ball = 1e-15,
                    iterate_monitor_name = 'IterateMonitor',
                    warmup_iter = 50),
                'change' : 0.5149233353418885 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', Nx = 256, lambda_0 = 0.75, lambda_max = 0.75,
                    TOL = 1e-08,
                    data_ball = 1e-15,
                    iterate_monitor_name = 'IterateMonitor'),
                'change' : 1.5660488308137295 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', Nx = 256, lambda_0 = 0.75, lambda_max = 0.75,
                    TOL = 1e-08,
                    data_ball = 1e-15,
                    iterate_monitor_name = 'IterateMonitor',
                    warmup_iter = 50),
                'change' : 0.5007359381993045 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', Nx = 256, lambda_0 = 1.0, lambda_max = 1.0,
                    TOL = 1e-08),
                'change' : 3.477353227160731 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DRl', Nx = 256, lambda_0 = 1.0, lambda_max = 1.0,
                    TOL = 1e-08,
                    warmup_iter = 50),
                'change' : 1.4031179750166034 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', Nx = 256, TOL = 1e-08, data_ball = 1e-15),
                'change' : 0.0002014035044529032 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', Nx = 256, TOL = 1e-08, data_ball = 1e-15,
                    warmup_iter = 50),
                'change' : 0.00023192078972714304 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', Nx = 256, TOL = 1e-08, data_ball = 1e-15,
                    accelerator_name = 'GenericAccelerator'),
                'change' : 0.6611496965114031 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'AvP', Nx = 256, TOL = 1e-08, data_ball = 1e-15,
                    accelerator_name = 'GenericAccelerator',
                    warmup_iter = 50),
                'change' : 0.5727674950553965 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', Nx = 256, TOL = 1e-08),
                'change' : 0.41759081178705976 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', Nx = 256, TOL = 1e-08, warmup_iter = 50),
                'change' : 0.3664417194542883 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger', Nx = 256, TOL = 1e-08),
                'change' : 0.05340217059183847 },
            { 'experiment' : 'CDP_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger', Nx = 256, TOL = 1e-08, warmup_iter = 50),
                'change' : 0.11687669563729032 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'ADMM', lambda_0 = 3.0, lambda_max = 3.0),
                'change' : 0.007991037992392957 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'ADMM', lambda_0 = 3.0, lambda_max = 3.0, noise = True,
                    rotate = True),
                'change' : 0.008831951107154882 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AP'),
                'change' : 0.2025744888623074 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP'),
                'change' : 0.2025744888623074 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75),
                'change' : 0.10033048883397686 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP2', lambda_0 = 0.75, lambda_max = 0.75, noise = True),
                'change' : 0.12340035109401419 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP', TOL = 5e-15, noise = True),
                'change' : 0.186175857609644 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CADRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0,
                    rotate = True),
                'change' : 0.34290497919411855 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CADRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0,
                    noise = True,
                    rotate = True),
                'change' : 0.5110526901544324 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0),
                'change' : 0.4839176611967821 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.25, lambda_max = 0.25,
                    rotate = True),
                'change' : 0.14598194469931863 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.25, lambda_max = 0.25,
                    noise = True,
                    rotate = True),
                'change' : 0.32268316184971646 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1.0, lambda_max = 1.0,
                    noise = True),
                'change' : 0.1271194493765856 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CP', formulation = 'cyclic', TOL = 5e-15, rotate = True),
                'change' : 0.23770572319104438 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'CP', formulation = 'cyclic', noise = True, rotate = True),
                'change' : 0.28702788642808297 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.65, lambda_max = 0.65),
                'change' : 0.571163776103382 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, noise = True),
                'change' : 0.22755370213962048 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.55, lambda_max = 0.55, anim = True),
                'change' : 0.22824296617383394 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.55, lambda_max = 0.55, noise = True),
                'change' : 0.2231095720826942 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, noise = True),
                'change' : 1.2391032782661446 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', TOL = 5e-09, anim = True),
                'change' : 0.0004737248320938118 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'DyRePr', TOL = 5e-09, noise = True),
                'change' : 0.000472063693767594 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator'),
                'change' : 0.3221295624272043 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', noise = True),
                'change' : 0.30576306861981306 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'HPR', lambda_0 = 0.85, lambda_max = 0.85, anim = True),
                'change' : 0.8257368969902201 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'PHeBIE', anim = True),
                'change' : 0.30776833951724303 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'PHeBIE', anim = True, noise = True),
                'change' : 0.3136466105969366 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 0.25377823684639794 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', noise = True),
                'change' : 0.23761737552201437 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'RRR', lambda_0 = 0.95, lambda_max = 0.95, anim = True),
                'change' : 1.031213258205757 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'RRR', lambda_0 = 0.95, lambda_max = 0.95, noise = True),
                'change' : 1.119230890213313 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger'),
                'change' : 0.0499907880744614 },
            { 'experiment' : 'JWST_Experiment' ,
                'args' : getDict(algorithm = 'Wirtinger', noise = True),
                'change' : 0.049914482007897526 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'AvP', data_ball = 24),
                'change' : 0.005448342753224267 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'CP', data_ball = 20),
                'change' : 0.009297330050179218 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', data_ball = 20),
                'change' : 0.01031909839535757 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'DRl', data_ball = 20, lambda_0 = 0.5, lambda_max = 0.5),
                'change' : 0.01143704222984099 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'AvP'),
                'change' : 0.005448342753224267 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'CP'),
                'change' : 0.009297330050179218 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'DRAP'),
                'change' : 0.01031909839535757 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.5, lambda_max = 0.5),
                'change' : 0.01143704222984099 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 0.006183259667118777 },
            { 'experiment' : 'Krueger_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', data_ball = 20),
                'change' : 0.006183259667118777 },
            # # Tests disabled, because no data download facility yet
            # {'experiment': 'DegenerateOrbital',
            #     'args': getDict(algorithm='CP', constraint='sparse real', sparsity_parameter=180),
            #     'change': 0.0007831049810539593},
            # {'experiment': 'OrthogonalOrbitals',
            #     'args': getDict(algorithm='CP', constraint='sparse real'),
            #     'change': 0.0022726783274420216},
            {'experiment': 'PlanarMolecule',
                'args': getDict(algorithm='DRl', constraint='sparse real', sparsity_parameter=40),
                'change': 6.550158901826162},
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP', sensors = 10),
                'change' : 0.0018547339405736419 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3,
                    sensors = 10),
                'change' : 0.0003217086659208949 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3,
                    sensors = 10,
                    noise = True),
                'change' : 0.00044078827777552714 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1,
                    sensors = 10),
                'change' : 5.127407575407209e-09 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33,
                    sensors = 10),
                'change' : 5.548821069858784e-07 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33,
                    sensors = 10,
                    noise = True),
                'change' : 1.489905395818809e-06 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1,
                    sensors = 10,
                    noise = True),
                'change' : 2.3156949015949943e-09 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CP', sensors = 10),
                'change' : 0.0020467735303876327 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CP', sensors = 10, noise = True),
                'change' : 0.004570003108536397 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, sensors = 10),
                'change' : 0.007111991210504233 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, sensors = 10),
                'change' : 0.002115695526375216 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, sensors = 10,
                    noise = True),
                'change' : 0.005125745784275618 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.85, lambda_max = 0.85, sensors = 10),
                'change' : 0.004835390908593401 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.85, lambda_max = 0.85, sensors = 10,
                    noise = True),
                'change' : 0.0206750536251609 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, sensors = 10,
                    noise = True),
                'change' : 0.050693566902058444 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DyRePr', sensors = 10),
                'change' : 2.8217281768209766e-08 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DyRePr', sensors = 10, noise = True),
                'change' : 3.984508156135555e-07 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', sensors = 10),
                'change' : 0.0031823954438990926 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', sensors = 10, noise = True),
                'change' : 0.00786949180196369 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP'),
                'change' : 0.001116540575903532 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3),
                'change' : 0.0004803772226963815 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP2', formulation = 'NSLS', lambda_0 = 0.3, lambda_max = 0.3,
                    noise = True),
                'change' : 0.0009981138344866696 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1),
                'change' : 1.4354991468948208e-06 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33),
                'change' : 0.00014415396762188995 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 0.33, lambda_max = 0.33,
                    noise = True),
                'change' : 0.0015288604380690083 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CDRl', formulation = 'cyclic', lambda_0 = 1, lambda_max = 1,
                    noise = True),
                'change' : 4.420658004894132e-05 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CP', formulation = 'cyclic'),
                'change' : 0.00020071197630479806 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'CP', formulation = 'cyclic', noise = True),
                'change' : 0.0029751369217766235 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0),
                'change' : 0.005057809729781892 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25),
                'change' : 0.0010384409147302935 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.25, lambda_max = 0.25, noise = True),
                'change' : 0.0053860904510107666 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.85, lambda_max = 0.85),
                'change' : 0.003078078827674661 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 0.85, lambda_max = 0.85, noise = True),
                'change' : 0.012143474176243075 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DRl', lambda_0 = 1.0, lambda_max = 1.0, noise = True),
                'change' : 0.01846731838926987 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DyRePr'),
                'change' : 8.068274526875429e-10 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'DyRePr', noise = True),
                'change' : 7.678266822439317e-11 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator'),
                'change' : 0.0014407080042213249 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator', noise = True),
                'change' : 0.00839160852950864 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 0.0 },
            { 'experiment' : 'SourceLocExperiment' ,
                'args' : getDict(algorithm = 'QNAvP', noise = True),
                'change' : 0.0 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', TOL = 0.0005),
                'change' : 0.028415986028241903 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0, TOL = 0.0005),
                'change' : 0.04503436075145005 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CP', debug = False),
                'change' : 0.021585554496420627 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CP', cutoff = 20),
                'change' : 0.02677541008179117 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CP', cutoff = 25),
                'change' : 0.02392870645723383 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CP', cutoff = 3),
                'change' : 0.017501549264108456 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'CP', cutoff = 5),
                'change' : 0.06760865009032889 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'DRl', TOL = 0.0005),
                'change' : 0.28810260114003927 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'AvP', accelerator_name = 'GenericAccelerator'),
                'change' : 0.021168286958578565 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 0.06092110842117178 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', cutoff = 15),
                'change' : 0.06105147616607071 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', cutoff = 25),
                'change' : 0.03030461817389522 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', cutoff = 3),
                'change' : 0.055588085394952656 },
            { 'experiment' : 'Sparse2_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP', cutoff = 5),
                'change' : 0.05363280193067823 },
            { 'experiment' : 'SudokuExperiment' ,
                'args' : getDict(algorithm = 'DRl'),
                'change' : 0.18207260487950389 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'AvP'),
                'change' : 4.677844563925475 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0),
                'change' : 2.862467500047653 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'CDRl', lambda_0 = 0.9, lambda_max = 0.9),
                'change' : 3.6017082134509852 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'CP'),
                'change' : 3.678092798186146 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'DRAP', lambda_0 = 0.02, lambda_max = 0.02),
                'change' : 2.950664953648576 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'DRl'),
                'change' : 2.862467500047574 },
            { 'experiment' : 'CDI_Experiment' ,
                'args' : getDict(algorithm = 'QNAvP'),
                'change' : 8.256773112923149 }
            ]


    testEngine = TestEngine(testData, maxIterCount=5, maxError=1e-5)

    # to select the appropriate mode of execution of this script
    # uncomment the line you need, and comment out the others
    # mode = Mode.GenerateTestData
    # mode = Mode.UpdateTestData
    mode = Mode.RunTests

    t = time.time()
    if mode == Mode.GenerateTestData:
        testEngine.generateTestData()
    elif mode == Mode.UpdateTestData:
        testEngine.updateTestData()
    else: #Mode.RunTests
        success = testEngine.run()
        if success:
            print("All", len(testEngine.testData), "tests were successful.")
        else:
            print("Regression test failed:", testEngine.failedCount, "demo(s) failed.")
    print("Took", time.time() - t, "seconds.")
