
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='CP', formulation='cyclic')
sourceExp.run()
sourceExp.show()
