
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='AvP2', lambda_0=0.75, lambda_max=0.75, noise=True)
JWST.run()
JWST.show()
