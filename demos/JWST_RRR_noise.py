
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='RRR', lambda_0=0.95, lambda_max=0.95, noise=True)
JWST.run()
JWST.show()
