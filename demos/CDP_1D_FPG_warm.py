
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

# Note: This demo does not converge (it blows up, same with ProxMatlab)

CDP = CDP_Experiment(algorithm='AvP', constraint='support only',
                     accelerator_name='GenericAccelerator', warmup_iter=50)
CDP.run()
CDP.show()
