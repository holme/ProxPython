
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='DRl', lambda_0=0.85, lambda_max=0.85, sensors=10)
sourceExp.run()
sourceExp.show()
