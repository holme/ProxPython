
import SetProxPythonPath
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'AvP', data_ball = 24)
Krueger.run()
Krueger.show()
