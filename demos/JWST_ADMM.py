
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='ADMM', lambda_0=4.0, lambda_max=4.0, anim=True)
JWST.run()
JWST.show()
