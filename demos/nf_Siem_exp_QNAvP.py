
import SetProxPythonPath
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'QNAvP', data_ball = 20)
Krueger.run()
Krueger.show()
