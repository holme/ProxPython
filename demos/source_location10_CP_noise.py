
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='CP', sensors=10, noise=True)
sourceExp.run()
sourceExp.show()
