
import SetProxPythonPath
from proxtoolbox.experiments.phase.Krueger_Experiment import Krueger_Experiment

Krueger = Krueger_Experiment(algorithm = 'CP')
Krueger.run()
Krueger.show()
