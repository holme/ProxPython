
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='DRl', Nx=256, lambda_0=0.75, lambda_max=0.75,
                     MAXIT=1000, TOL=1e-8, data_ball = 1e-15, iterate_monitor_name = 'IterateMonitor')
CDP.run()
CDP.show()
