
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='CDRl', formulation='cyclic',
                       lambda_0=1.0, lambda_max=1.0, MAXIT=300, noise=True)
JWST.run()
JWST.show()
