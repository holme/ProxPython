
import SetProxPythonPath
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment

phasepack = Phasepack_Experiment(algorithm='DRl', formulation='cyclic', 
                                 MAXIT=2000, lambda_0=0.7, lambda_max=0.7)
phasepack.run()
phasepack.show()
