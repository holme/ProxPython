
import SetProxPythonPath
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment

phasepack = Phasepack_Experiment(algorithm='DRAP', formulation='cyclic', 
                                 MAXIT=2000, lambda_0=0.02, lambda_max=0.02)
phasepack.run()
phasepack.show()
