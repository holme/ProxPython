
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

CDP = CDP_Experiment(algorithm='AvP', Nx=256, MAXIT=5000, TOL=1e-8, data_ball = 1e-15,
                     accelerator_name='GenericAccelerator', warmup_iter=50)
CDP.run()
CDP.show()
