import pdb
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='DRl', lambda_0=0.55, lambda_max=0.55, anim=True)
JWST.run()
JWST.show()
