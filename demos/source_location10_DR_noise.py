
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='DRl', lambda_0=1.0, lambda_max=1.0, sensors=10, noise=True)
sourceExp.run()
sourceExp.show()
