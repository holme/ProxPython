import sys, os
# add the demos directory into the Python's path
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../demos")

# run the demos
import nf_Siem_exp_noise_AvP
import nf_Siem_exp_noise_QNAvP
import nf_Siem_exp_noise_drl
import nf_Siem_exp_noise_DRAP
import nf_Siem_exp_noise_CP
