# ProxToolbox


[[_TOC_]]

The ProxToolbox is a collection of modules for solving mathematical problems
using fixed point iterations with proximal operators. It was used to generate
many if not all of the numerical experiments conducted in the papers in the
ProxMatlab Literature folder.

For a complete listing of papers with links, go to the [groups publications]
(https://vaopt.math.uni-goettingen.de/en/publications.php).

This site is maintained by the Working Group in Variational Analysis of the
Institute for Numerical and Applied Mathematics at the University of Göttingen.

![Video: Representative iterate of a noisy JWST test wavefront recovered with the Douglas-Rachford algorithm](./media/JWST.mp4)

## Python version

**1.0.5**:
* [Sources](https://gitlab.gwdg.de/nam/ProxPython/-/releases/1.0.5)
* [Documentation](https://num.math.uni-goettingen.de/proxtoolbox/versions/1.0.5)


The documentation includes a tutorial.

### Older versions

- **0.2.2**: [Source](https://gitlab.gwdg.de/nam/ProxPython/-/releases/0.2.2), [Documentation](https://num.math.uni-goettingen.de/proxtoolbox/versions/0.2)
- **0.1**: [Source](https://num.math.uni-goettingen.de/proxtoolbox/tarballs/0.1/proxpython-0.1.tar.gz), [Documentation](https://num.math.uni-goettingen.de/proxtoolbox/versions/0.1)


## Matlab version

[3.0](https://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Release3.0.tar.gz)

For help with the Matlab version, see the :code:`README` file in the ProxMatlab source.


## Additional binary data

* [Phase (28mb)](https://vaopt.math.uni-goettingen.de/data/Phase.tar.gz)
* [Ptychography (417mb)](https://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz)
* [CT (1.7mb)](https://vaopt.math.uni-goettingen.de/data/CT.tar.gz)

The binary data need to be unpacked in the `InputData` subdirectory source code.
For example, the `Phase` data in ProxPython would be in `ProxPython/InputData/Phase`.

## Acknowledgements

**Main Author**: Russell Luke, University of Göttingen

**Contributors**:

Contributors are from institute for Numerical and Applied Mathematics, University of Göttingen, if not stated otherwise.

Sylvain Gretchko (python); 
Jochen Schulz (python, administration and structure);
Matthijs Jansen, I. Institute for Physics, University of Göttingen (Orbital Tomography);
Alexander Dornheim (python);
Stefan Ziehe (python);
Rebecca Nahme (python);
Matthew Tam, CARMA, University of Newcastle, Australia (Ptychography);
Pär Mattson (Ptychography);
Robin Wilke, Inst. for X-Ray Physics, Univesität Göttingen (Ptychography and Phase);
Robert Hesse;
Alexander Stalljahn (Sudoku).

**Funding**:

This has grown over the years and has been supported in part by:

* NASA grant NGT5-66; 
* Pacific Institute for Mathematical Sciences (PIMS);
* USA NSF Grant DMS-0712796;
* German DFG grant SFB-755 TPC2;
* German Israeli Foundation (GIF) grant G-1253-304.6/2014.


# Getting started

Before you do anything, you need to load the following python packages:
        * numpy
        * matplotlib
        * iamgeio
        * h5py
        * urllib3 
        * numba 
        * scikitimage 
        * scikitlearn

The easiest way to use ProxPython is to run some of the included demos.
There are different families of experiments and various demos.
All the demos are located in the ProxPython/demos folder. For convenience,
all the Nanoscale Photonic Imaging demos have been grouped together
in the `ProxPython/Nanoscale_Photonic_Imaging` folder.

To run a demo, open a command line. Assuming ProxPython is in your current folder,
change your current folder to demos by

``` bash
cd ProxPython/demos
```

Then just type


``` bash
python3 demo_name
```

to run the demo called "demo_name".

For example,

``` bash
python3 JWST_DRl.py
```

will run the DRl algortihm on the JWST experiment.

## Structure

What is the basic structure of ProxPython?

### I. proxtoolbox folder

This is the heart of the proxtoolbox. Most of the code is found here.
There are four subfolders:

1. experiments

    This folder contains the different experiments to which the
    proxtoolbox can be applied. It defines the abstract Experiment
    class which contains all the information that describes a given
    experiment. Essentially, this class loads or generates the data
    used in the experiment and instanciates the algorithm that works on
    this data. This class is meant to be derived from to implement
    concrete experiments.
    There are currently four families of experiments: the phase retrieval
    experiments, computed tomography (CT), ptychography, and Sudoku.
    The orbital tomography experiment will be integrated soon.

2. algorithms

    The proxtoolbox allows the use of different algortihms.
    This folder contains the abstract Algorithm class and
    various concrete classes.
    At moment the following algortihms have been implemented:

     - AP:   Alternating Projections
     - AvP:  Averaged Projections
     - CDRl: relaxed version of a cyclic averaged
             alternating reflections method (CDR) proposed by
             Borwein and Tam, Journal of Nonlinear and Convex Analysis
             Volume 16, Number 4.  The relaxation (CDRl) is the cyclic 
             implementation of the relaxed Douglas Rachford algorithm proposed 
             by Luke.  The convex case for CDRL was studied in 
             "Relaxed Cyclic Douglas-Rachford Algorithms for Nonconvex 
             Optimization", D. R. Luke, A. Martins and M. K. Tam. 
             ICML 2018 Workshop. Stockholm, July 2018 
             (https://sites.google.com/view/icml2018nonconvex/papers) and 
             the performance on phase retrieval was studied in 
             D. R. Luke, S. Sabach and M. Teboulle , SIAM J. Mathematics of 
             Data Science, 1(3), 408-445 (2019). 
     - CP:   Cyclic Projections
     - DRAP: a type of hybrid Douglas-Rachford and Alternating projections
             proposed by Nguyen H. Thao, ``A convergent relaxation of the
             Douglas--Rachford algorithm", Comput. Optim. Appl., 70
             (2018), pp. 841--863.
     - DRl:  Douglas-Rachford-lambda (same as RAAR below)
     - HPR:  (Heinz-Patrick-Russell algorithm.
             See Bauschke,Combettes&Luke, Journal of the Optical
             Society of America A, 20(6):1025-1034 (2003))
     - RRR: Relaxed-Reflect-Reflect algorithm by Veit Elser, 
            IEEE Trans. Inform. Theory, 64 (2018), pp. 412–428.
     - PHeBIE: Proximal Heterogenious Block Implicit-Explicit (PHeBIE)
             minimzation algorithm as proposed in the paper
             "Proximal Heterogeneous Block Implicit-Explicit Method and
             Application to Blind Ptychographic Diffraction Imaging",
             R. Hesse, D. R. Luke, S. Sabach and M. K. Tam,
             SIAM J. on Imaging Sciences, 8(1):426--457 (2015))
     - QNAvP: Quasi-Newton Accelerated Averaged Projections
     - RAAR: Relaxed Averaged Alternating Reflection algorithm.
             For background see:
             D.R.Luke, Inverse Problems 21:37-50(2005)
             D.R. Luke, SIAM J. Opt. 19(2): 714--739 (2008))


3. proxoperators
    Contains the prox operators classes used by the algorithms.


4. utils
    Contains some utilities, mostly for loading and processing data.

### II. InputData

When you first download the proxtoolbox this folder should be empty.
When you call a ART/Phase/Ptychography demo the first time, you are asked if you
want to automatically download the InputData for ART, phase or ptychography.
If this does not work you need to download the data manually from:

* [Phase (28mb)](https://vaopt.math.uni-goettingen.de/data/Phase.tar.gz)
* [Ptychography (417mb)](https://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz)
* [CT (1.7mb)](https://vaopt.math.uni-goettingen.de/data/CT.tar.gz)

Extract the data and store it in
``` bash
InputData/Phase/
InputData/Ptychography/
InputData/CT/
```

### III. Demos

All the demos are located in the demos folder. For convenience,
all the Nanoscale Photonic Imaging demos have been grouped together
in the ProxPython/Nanoscale_Photonic_Imaging folder. To run a demo,
change your current folder to demos or Nanoscale_Photonic_Imaging and
type

``` bash
python3 demo_name
```

#### Demos Accompanying Publications:

- [Cone and Sphere benchmark](https://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Cone_and_Sphere.tar.gz):
  The demos are in the subfolder Cone_and_Sphere.  These are the codes generating the benchmarks in the article
  [Optimization on Spheres: Models and Proximal Algorithms with Computational Performance Comparisons](https://epubs.siam.org/doi/abs/10.1137/18M1193025)
  by D.R. Luke, S. Sabach and M. Teboulle, SIAM J. Mathematics of Data Science, 1(3), 408-445 (2019).


- Nanoscale Photonic Imaging demos:
  The demos [(Matlab)](https://vaopt.math.uni-goettingen.de/en/software/ProxMatlab-Cone_and_Sphere.tar.gz) 
  [(Python)](https://gitlab.gwdg.de/nam/ProxPython/-/releases/1.0.3) are in the subfolder Nanosclae_Photonic_Imaging.  These are the codes accompanying the tutorial chapter by D. R. Luke
  'Proximal Methods for Image Processing' pp. 165-202 (2020).
  In T. Salditt, A. Egner and D.R. Luke (eds)  [Nanoscale Photonic Imaging](https://link.springer.com/book/10.1007/978-3-030-34413-9).
  Topics in Applied Physics, vol 134. Springer, Cham.
